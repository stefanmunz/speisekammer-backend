import express from "express";
import bodyParser from "body-parser";
import cors from "cors";
import helmet from "helmet";
import compression from "compression";
import rateLimit from "express-rate-limit";
import { body, check, validationResult } from "express-validator";
import pool from "./config";

const app = express();
app.use(compression());
app.use(helmet());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const origin = {
  origin: pool.isProduction ? "https://speisekammer-backend.herokuapp.com" : "*"
};
app.use(cors(origin));

const limiter = rateLimit({
  windowMs: 1 * 60 * 1000, // 1 minute
  max: 5 // 5 requests,
});
app.use(limiter);

app.get("/", (request: any, response: any) => {
  response.json({ info: "Node.js, Express, and Postgres API" });
});

app.get("/recipes", (request: any, response: any) => {
  pool.query("SELECT * FROM recipes", (error: Error, results: any) => {
    if (error) {
      throw error;
    }
    response.status(200).json(results.rows);
  });
});

app.post(
  "/recipes",
  [
    check("title")
      .not()
      .isEmpty()
      .isLength({ min: 5, max: 255 })
      .trim()
  ],
  (request: any, response: any) => {
    const { title, comment } = request.body;

    pool.query(
      "INSERT INTO recipes (title, comment) VALUES ($1, $2)",
      [title, comment],
      (error: Error) => {
        if (error) {
          throw error;
        }
        response
          .status(201)
          .json({ status: "success", message: "Recipe added." });
      }
    );
  }
);

app.get("/users", (request: any, response: any) => {
  pool.query(
    "SELECT * FROM users ORDER BY id ASC",
    (error: Error, results: any) => {
      if (error) {
        throw error;
      }
      response.status(200).json(results.rows);
    }
  );
});

app.post(
  "/users",
  [
    check("firstname")
      .not()
      .isEmpty()
      .isLength({ min: 5, max: 255 })
      .trim(),
    check("email")
      .not()
      .isEmpty()
      .isLength({ min: 5, max: 255 })
      .trim()
  ],
  (request: any, response: any) => {
    const errors = validationResult(request);

    if (!errors.isEmpty()) {
      return response.status(422).json({ errors: errors.array() });
    }

    const { firstname, email } = request.body;

    pool.query(
      "INSERT INTO users (firstname, email) VALUES ($1, $2)",
      [firstname, email],
      (error: Error, results: any) => {
        if (error) {
          throw error;
        }
        response.status(201).send(`User added with ID: ${results.insertId}`);
      }
    );
  }
);

app
  .route("/users/:id")
  .get((request: any, response: any) => {
    const id = parseInt(request.params.id);

    pool.query(
      "SELECT * FROM users WHERE id = $1",
      [id],
      (error: Error, results: any) => {
        if (error) {
          throw error;
        }
        response.status(200).json(results.rows);
      }
    );
  })
  .put((request: any, response: any) => {
    const id = parseInt(request.params.id);
    const { name, email } = request.body;

    pool.query(
      "UPDATE users SET name = $1, email = $2 WHERE id = $3",
      [name, email, id],
      (error: Error, results: any) => {
        if (error) {
          throw error;
        }
        response.status(200).send(`User modified with ID: ${id}`);
      }
    );
  })
  .delete((request: any, response: any) => {
    const id = parseInt(request.params.id);

    pool.query(
      "DELETE FROM users WHERE id = $1",
      [id],
      (error: Error, results: any) => {
        if (error) {
          throw error;
        }
        response.status(200).send(`User deleted with ID: ${id}`);
      }
    );
  });

// Start server
app.listen(process.env.PORT || 3002, () => {
  console.log(`Server listening on port 3002`);
});
