CREATE TABLE users (
  ID SERIAL PRIMARY KEY,
  firstname VARCHAR(255),
  email VARCHAR(255)
);

INSERT INTO users (firstname, email)
  VALUES ('Jerry', 'jerry@example.com'), ('George', 'george@example.com');
  
CREATE TABLE recipes ( 
  ID SERIAL PRIMARY KEY, 
  title VARCHAR(255) NOT NULL, 
  weblink VARCHAR(255), 
  imagelink VARCHAR(255), 
  comment TEXT
);

INSERT INTO recipes (title, comment) 
  VALUES ('Hünherfrikassee', 'Das nächste Mal Zitronensaft nehmen anstatt Essig'), 
  ('Pommes', 'Entenfett anstatt Sonnenblumenöl nehmen');
